--validate the data pushed to loading zone. Make sure the counts are matching with the staging
--CUSTOMER VALIDATION
SELECT  COUNT(*) FROM LOBC_PARTY_LZ where store_id=STORE_ID;
--CLUB VALIDATION
SELECT  COUNT(*) FROM LOBC_CLUB_LZ where store_number=store_number;
--DEPOSITS VALIDATION
SELECT  COUNT(*) FROM LOBC_DEPOSITS_LZ where store_id=STORE_ID;
-- INVENTORY VALIDATION
SELECT COUNT(*) FROM LOBC_INVENTORY_LZ where store_number=store_number;
-- AGREEMENT VALIDATION
SELECT  COUNT(*) FROM LOBC_AGREEMENT_LZ where store_id=STORE_ID;
-- PAYMENT VALIDATION
SELECT COUNT(*) FROM  LOBC_PAYMENTS_LZ where store_id=STORE_ID;
-- CALL VALIDATION
SELECT COUNT(*) FROM LOBC_CALL_MGMT_LZ where store_number=store_number;